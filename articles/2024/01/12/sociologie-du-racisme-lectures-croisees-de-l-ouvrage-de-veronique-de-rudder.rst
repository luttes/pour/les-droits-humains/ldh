

.. _ldh_2024_01_12:

=================================================================================================
2024-01-12 **Sociologie du racisme » : lectures croisées de l'ouvrage de Véronique De Rudder**
=================================================================================================

- https://www.syllepse.net/sociologie-du-racisme-_r_46_i_790.html

.. tags:: Sociologie

Voici une information sur une journée d’étude organisée par l’URMIS
laboratoire à l’Université Paris Diderot le vendredi 12 janvier 2024
sur le thème : « Sociologie du racisme » : lectures croisées de l'ouvrage
de Véronique De Rudder. Cette journée d’étude peut, également, être
suivie en visio, voici le programme ci-dessous. 

Journée d'études intitulée "« Sociologie du racisme » : lectures croisées
de l'ouvrage de Véronique De Rudder" se déroulera le 12 janvier prochain à
partir de 9h30, salle 105, Olympe de Gouges (Université Paris Cité).

Le racisme et les discriminations sont un système. Véronique De ­Rudder
nous en dévoile ici les mécanismes et passe au crible les relations
­inter-ethniques qui en découlent. Elle explore la place de l’immigration et
de sa descendance dans la société française.  Ses textes s’avèrent d’une
étonnante actualité, alors même que les ­enfants d’immigrés, désormais
adultes, sont porteurs de revendications d’égalité.  Elle nous propose
une analyse critique du ­républicanisme français dont l’universalisme,
­inscrit en lettres d’or dans les textes ­constitutionnels, coïncide en
pratique avec un système de discriminations tolérées, voire, à l’occasion,
codifiées.  Les victimes du racisme sont massivement les ­immigrés originaires
des anciennes colonies et leurs enfants, citoyens français de plein droit,
et pourtant de seconde zone, renvoyés à leurs origines comme à une marque
d’indignité.

Se réclamant d’un universalisme en actes, l’auteure souligne la nécessité
de changer les politiques qui malmènent les valeurs démocratiques.

Une participation à distance est prévue :
https://u-paris.zoom.us/j/81118555689?pwd=UG5hdWdtakxHUE1YemdOdk9YR2Jldz09

ID de réunion: 811 1855 5689 Code secret: 597086

Cette Journée scientifique a pour objectif de débattre de l'ouvrage de
Véronique de Rudder, "Sociologie du racisme", qui est paru à titre posthume
à la fin 2019 aux éditions Syllepse. L'ouvrage réunit une sélection de
17 textes, sélectionnés et introduits par les éditrices Maryse Tripier,
Catherine Quiminal, Aude Rabaud, Mireille Eberhard, Marguerite Cognet, textes
répartis en trois parties : "Logement et cohabitation pluri-ethnique" ;
"Problèmes épistémologiques, processus de catégorisation, précautions
d'"usage""; "Racisme et discriminations".

La discussion de cet ouvrage sera assurée par des collègues spécialistes
de ces questions qui présenteront un texte de l'une des trois parties qui
composent le livre et en proposeront une lecture critique en prenant appui
sur leurs propres travaux.

Programme 
=============

9h30 : Introduction par les éditrices de l'ouvrage 
-----------------------------------------------------------------------------

Maryse Tripier, Catherine Quiminal, Aude Rabaud, Mireille Eberhard, Marguerite Cognet

10h-12h30 : "Logement et cohabitation pluri-ethnique"
--------------------------------------------------------------

Violette ARNOULET (sociologue, associée Lab'urba), Pierre GILBERT (sociologue,
maître de conférences université Paris‑8, CRESPPA–CSU),  Janoé VULBEAU
(historien et politiste, associé CERAPS)

Après-midi 13h30-17h
=========================

13h30 : Introduction Maryse Tripier
----------------------------------------

“Problèmes épistémologiques, processus de catégorisation, précautions
d'"usage"”

Narguesse KEYHANI (sociologue, Maîtresse de conférences Université Lumière
Lyon-2, Triangle), Louise VIROLE (sociologue, Maîtresse de conférences
Université Paris Cité URMIS, fellow ICM)

“Racisme et Discriminations”

Rachid BOUCHAREB (sociologue, chercheur au Centre Pierre Naville),  Damien
TRAWALE (sociologue, post doctorant INED, associé URMIS)

17h : clôture de la journée
----------------------------------


Rappel : participation à distance 
=====================================

- https://u-paris.zoom.us/j/81118555689?pwd=UG5hdWdtakxHUE1YemdOdk9YR2Jldz09

ID de réunion: 811 1855 5689 Code secret: 597086

Au plaisir de vous y retrouver,
Et avec tous nos bons voeux pour la nouvelle année,

Jean-Luc Primon,

Pour le comité d'organisation (Géraldine Bozec, Mireille Eberhard, Jean-Luc
Primon, Aude Rabaud)

